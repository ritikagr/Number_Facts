package com.codeflight.ritik.whatnumbersays;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    private RequestQueue mRequestQueue;
    private String result = null;
    private boolean flagAdv = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRequestQueue = Volley.newRequestQueue(this);
    }

    private void createRequest(String url)
    {
        final ProgressDialog p = new ProgressDialog(this);
        p.setMessage("Fetching Fact...");
        p.setCancelable(false);
        p.setCanceledOnTouchOutside(false);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        p.dismiss();
                        showFact(response);
                        Log.i(getString(R.string.TAG), response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        p.dismiss();
                        showFact(error.getMessage());
                    }
                });

        stringRequest.setTag(getString(R.string.TAG));
        p.show();
        mRequestQueue.add(stringRequest);
    }

    private void getResult(final String type, boolean flag)
    {
        if(flag)
        {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Enter Any Number");

            LayoutInflater inflater = this.getLayoutInflater();
            final View view = inflater.inflate(R.layout.dialog_activity, null);
            final EditText et = (EditText) view.findViewById(R.id.any_number);
            if(type == getString(R.string.date)) {
                et.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
                et.setHint("Enter date in mm/dd format");
            }

            builder.setView(view)
                    .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(et.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            if(et.getText().toString().trim().isEmpty())
                                showFact(getString(R.string.pean));
                            else
                                createRequest(getString(R.string.url2) + et.getText().toString().trim() + "/" + type);
                            dialogInterface.cancel();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

            AlertDialog b = builder.create();
            b.setCancelable(false);
            b.setCanceledOnTouchOutside(false);
            b.show();
        }
        else
            createRequest(getString(R.string.url1) + type);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mRequestQueue!=null)
        {
            mRequestQueue.cancelAll(getString(R.string.TAG));
        }
    }

    public void TriviaFacts(View view)
    {
        getResult(getString(R.string.trivia), flagAdv);
    }

    public void MathFacts(View view)
    {
        getResult(getString(R.string.math), flagAdv);
    }

    public void DateFacts(View view)
    {
        getResult(getString(R.string.date), flagAdv);
    }

    public void YearFacts(View view)
    {
        getResult(getString(R.string.year), flagAdv);
    }

    public void AdvancedVersionTab(View view)
    {
        Button bt = (Button) findViewById(R.id.advanced_version);
        if(flagAdv)
        {
            flagAdv = false;
            bt.setText(getString(R.string.sta));
            Toast.makeText(this, "UI Switched to Basic Version", Toast.LENGTH_SHORT).show();
        }
        else
        {
            flagAdv = true;
            bt.setText(getString(R.string.stb));
            Toast.makeText(this, "UI Switched to Advanced Version", Toast.LENGTH_SHORT).show();
        }
    }

    private void showFact(String fact)
    {
        if(!(fact==getString(R.string.pean))) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(fact);
            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            AlertDialog b = builder.create();
            b.setCancelable(false);
            b.setCanceledOnTouchOutside(false);
            b.show();
        }
        else
            Toast.makeText(this, fact, Toast.LENGTH_SHORT).show();
    }
}
